---
title: Git of Projects Directory
---

Sourcehut

* [sourcehut](https://sr.ht/~sircmpwn/sourcehut/)

Gitlab

* [rwx.gg](https://gitlab.com/rwx.gg)
* [pegn](https://gitlab.com/pegn)
* [anarch](https://gitlab.com/drummyfish/anarch)
* [smallchesslib](https://gitlab.com/drummyfish/smallchesslib)

Github

* [linux](https://github.com/torvalds/linux)
* [git](https://github.com/git)
* [golang](https://github.com/golang/)
* [tmux](https://github.com/tmux/)
* [vim](https://github.com/vim/)
* [alacritty](https://github.com/alacritty/)
* [vscodium](https://github.com/VSCodium/vscodium)
* [chocolate-doom](https://github.com/chocolate-doom/)
* [crispy-doom](https://github.com/fabiangreffrath/crispy-doom)
* [freedoom](https://github.com/freedoom/freedoom)
* [pandoc](https://github.com/jgm/pandoc)
